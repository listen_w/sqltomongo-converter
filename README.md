# sqltomongo-converter

#### 介绍
sql 语法转化为 mongo 语法核心转化器


#### 使用说明

1. 代码托管在 Maven 中央仓库中
```
<dependency>
  <groupId>cn.org.wangchangjiu</groupId>
  <artifactId>sqltomongo-converter</artifactId>
  <version>{sqltomongo.version}</version>
</dependency>
```


2. 利用 `SelectSQLTypeParser` 静态方法 `SelectSQLTypeParser.defaultConverterToString(String sql)` 转换；

例：
```
String sql = "SELECT n._id, n.languageList, n.score, ne.enable FROM novel n left join novelEpisodes ne on n._id = objectId(ne.novelId) limit 0,10"
String mongo = SelectSQLTypeParser.defaultConverterToString(sql)

```

#### 限制说明：
- where后面的表达式只支持这种格式：a.id=1

- 只支持左连接和内连接
- 连接部分的函数只支持单边进行string()或者ObjectId()
    ```
    不支持：SELECT * FROM table_a as a JOIN table_b as b on string(b.a_id)=string(a.id) 
    支持：SELECT * FROM table_a as a JOIN table_b as b on string(b.a_id)=a.id
    ```
- 连表只支持等值连接,并且匹配条件只有一个
  ```
  不支持：SELECT * FROM table_a as a JOIN table_b as b on b.a_id=a.id and a.id=1 
  支持：SELECT * FROM table_a as a JOIN table_b as b on b.a_id=a.id
  ```
- 连表时，只能从主表关联到其他表，不能a表连b表，b表在连c表
    ```
    不支持：SELECT * FROM table_a as a JOIN table_b as b on b.a_id=a.id JOIN table_c as c on b.id=c.b_id
    支持：SELECT * FROM table_a as a JOIN table_b as b on b.a_id=a.id JOIN table_c as c on a.id=c.a_id
    ```
  
- 不支持数据库层面的操作，如创建数据库，修改数据库等
  
- 建表时，mongo不支持小数点个数的约束
- 建表时，mongo不支持某个字段自动增长
- 建表时，索引只支持单独声明,输出的mongo语句有多条用;隔开
```
不支持：
CREATE TABLE employees (
    employee_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
);
支持：
CREATE TABLE employees (
    employee_id INT NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (employee_id)
);
```
- 建表时，mongo不支持bigint、time、year、ENUM、SET、bit、blob
