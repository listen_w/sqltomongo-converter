package cn.org.wangchangjiu.sqltomongo.core.parser.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class MatchExpression {
    private List<MatchData> matchDataList;

    public MatchExpression(List<MatchData> matchDataList) {
        this.matchDataList = matchDataList.stream()
                .sorted(Comparator.comparing(MatchData::getPriority)
                        .reversed().thenComparing(MatchData::getSort))
                .collect(Collectors.toList());
    }
}
