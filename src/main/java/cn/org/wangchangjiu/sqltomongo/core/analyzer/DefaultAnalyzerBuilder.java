package cn.org.wangchangjiu.sqltomongo.core.analyzer;

import cn.org.wangchangjiu.sqltomongo.core.analyzer.select_option.*;
import cn.org.wangchangjiu.sqltomongo.core.analyzer.table_option.CreateTableColumnAnalyzer;

/**
 * @Classname DefaultAnalyzerBuilder
 * @Description
 * @Date 2022/12/2 17:56
 * @Created by wangchangjiu
 */
public class DefaultAnalyzerBuilder implements AnalyzerBuilder {

    @Override
    public Analyzer newSelectAnalyzerInstance() {
        return new AbstractAnalyzer.Builder()
                .addAnalyzer(new JoinAnalyzer())
                .addAnalyzer(new MatchAnalyzer())
                .addAnalyzer(new GroupAnalyzer())
                .addAnalyzer(new HavingAnalyzer())
                .addAnalyzer(new SortAnalyzer())
                .addAnalyzer(new LimitAnalyzer())
                .addAnalyzer(new ProjectAnalyzer())
                .build();
    }

    @Override
    public CreateTableColumnAnalyzer newCreateTableAnalyzerInstance() {
        return new CreateTableColumnAnalyzer();
    }
}
