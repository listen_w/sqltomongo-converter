package cn.org.wangchangjiu.sqltomongo.core.analyzer.select_option;

import cn.org.wangchangjiu.sqltomongo.core.analyzer.AbstractAnalyzer;
import cn.org.wangchangjiu.sqltomongo.core.parser.data.LookUpData;
import cn.org.wangchangjiu.sqltomongo.core.parser.data.MatchData;
import cn.org.wangchangjiu.sqltomongo.core.parser.data.PartSQLParserData;
import cn.org.wangchangjiu.sqltomongo.core.parser.data.TableNameManager;
import org.apache.commons.collections4.CollectionUtils;
import org.bson.Document;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Classname MatchAnalyzer
 * @Description
 * @Date 2022/8/12 11:53
 * @Created by wangchangjiu
 */
public class MatchAnalyzer extends AbstractAnalyzer {


    @Override
    public void proceed(List<Document> documents, PartSQLParserData data) {
        List<MatchData> matchData = data.getMatchData();
        // 分析 匹配 过滤  构建 mongo API
        documents.addAll(analysisMatch(matchData, data.getTableNameManager()));

    }

    /**
     * 分析 过滤 匹配条件 构建 mongo 过滤 API
     *
     * @param matchData
     * @return
     */
    protected List<Document> analysisMatch(List<MatchData> matchData, TableNameManager tableNameManager) {
        List<Document> documents = new ArrayList<>();
        Document document = ExpressionAnalyzer.buildExpressions(matchData, tableNameManager,false);

        if (document!=null) {
            // $match
            documents.add(new Document("$match", document));
        }
        return documents;
    }

}
