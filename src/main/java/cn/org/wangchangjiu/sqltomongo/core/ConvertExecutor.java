package cn.org.wangchangjiu.sqltomongo.core;

import org.bson.conversions.Bson;

/**
 * 装换执行器
 */
public interface ConvertExecutor {
    String execute(String sql);
    Bson executeForBson(String sql);
}
