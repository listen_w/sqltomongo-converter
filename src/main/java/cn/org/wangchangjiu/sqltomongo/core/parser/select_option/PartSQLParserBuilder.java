package cn.org.wangchangjiu.sqltomongo.core.parser.select_option;

import cn.org.wangchangjiu.sqltomongo.core.common.ParserPartTypeEnum;
import cn.org.wangchangjiu.sqltomongo.core.parser.select_option.PlainSelectPartSQLParser;

/**
 * @Classname PartSQLParserFactory
 * @Description
 * @Date 2022/12/1 16:27
 * @Created by wangchangjiu
 */
public interface PartSQLParserBuilder {

    PlainSelectPartSQLParser getPartSQLParserInstance(ParserPartTypeEnum item);


}
