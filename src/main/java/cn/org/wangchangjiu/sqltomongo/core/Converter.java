package cn.org.wangchangjiu.sqltomongo.core;

import cn.org.wangchangjiu.sqltomongo.core.exception.SqlParserException;
import cn.org.wangchangjiu.sqltomongo.core.util.SqlCommonUtil;
import net.sf.jsqlparser.JSQLParserException;
import org.bson.conversions.Bson;

public class Converter {

    static ConvertExecutorFactory FACTORY = new ConvertExecutorFactory();

    /**
     * 转换
     * @param sql sql
     * @return string mongo
     */
    public static String execute(String sql){
        SqlCommonUtil.SqlType type;
        try {
            type = SqlCommonUtil.getSqlType(sql);
        } catch (JSQLParserException exception) {
            throw new SqlParserException("不能获取SQL类型，解析SQL失败：", exception.getCause());
        }
        return FACTORY.getExecutor(type).execute(sql);
    }

    /**
     * 转换
     * @param sql sql
     * @return bson mongo
     */
    public static Bson executeForBson(String sql){
        SqlCommonUtil.SqlType type;
        try {
            type = SqlCommonUtil.getSqlType(sql);
        } catch (JSQLParserException exception) {
            throw new SqlParserException("不能获取SQL类型，解析SQL失败：", exception.getCause());
        }
        return FACTORY.getExecutor(type).executeForBson(sql);
    }
}
