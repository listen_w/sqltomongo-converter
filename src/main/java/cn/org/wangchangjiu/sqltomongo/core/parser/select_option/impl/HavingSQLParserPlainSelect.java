package cn.org.wangchangjiu.sqltomongo.core.parser.select_option.impl;

import cn.org.wangchangjiu.sqltomongo.core.adapter.MyExpressionVisitorAdapter;
import cn.org.wangchangjiu.sqltomongo.core.common.ParserPartTypeEnum;
import cn.org.wangchangjiu.sqltomongo.core.parser.data.GroupData;
import cn.org.wangchangjiu.sqltomongo.core.parser.data.MatchData;
import cn.org.wangchangjiu.sqltomongo.core.parser.data.PartSQLParserData;
import cn.org.wangchangjiu.sqltomongo.core.parser.select_option.PlainSelectPartSQLParser;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.statement.select.PlainSelect;
import org.apache.commons.lang3.ObjectUtils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class HavingSQLParserPlainSelect extends PlainSelectPartSQLParser {

    private List<MatchData> parser(Expression having, List<String> groupByNames) {
        if (ObjectUtils.isNotEmpty(having)) {
            List<MatchData> matchData = new ArrayList<>();
            having.accept(new MyExpressionVisitorAdapter(ParserPartTypeEnum.HAVING, matchData));
            matchData = matchData.stream()
                    .peek(d->{
                        if(!d.getIsOperator() && groupByNames.contains(d.getOriginExpressionField())){
                            d.setHavingGroupCollectionName("_id");
                        }
                    })
                    .sorted(Comparator.comparing(MatchData::getPriority)
                            .reversed().thenComparing(MatchData::getSort))
                    .collect(Collectors.toList());
            return matchData;
        }
        return new ArrayList<>();
    }


    @Override
    public void proceedData(PlainSelect plain, PartSQLParserData data) {
        List<String> groupByNames = data.getGroupData().stream().map(GroupData::getField).collect(Collectors.toList());
        List<MatchData> matchData = this.parser(plain.getHaving(),groupByNames);
        data.setHavingData(matchData);
    }
}
