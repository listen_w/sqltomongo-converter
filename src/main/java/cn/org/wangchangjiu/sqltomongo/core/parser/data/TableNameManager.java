package cn.org.wangchangjiu.sqltomongo.core.parser.data;

import lombok.Data;
import net.sf.jsqlparser.schema.Column;

import java.util.Map;
import java.util.Objects;

/**
 * 表名管理
 */
@Data
public class TableNameManager {
    /**
     *  主表别名
     */
    private String majorTableAlias;
    private Map<String, LookUpData> joinTables;

    public Object addTableName(Object value) {
        if (value instanceof Column && joinTables!=null) {
            Column column = (Column) value;
            String originName = column.getTable().getName();
            LookUpData joinTable = joinTables.get(originName);
            if (!Objects.equals(originName, majorTableAlias) && joinTable !=null) {
                return "$".concat(value.toString().replaceAll(originName,joinTable.getAs()));
            }else {
                return "$".concat(value.toString().replaceAll(originName.concat("."),""));
            }
        }
        return value;
    }
}
