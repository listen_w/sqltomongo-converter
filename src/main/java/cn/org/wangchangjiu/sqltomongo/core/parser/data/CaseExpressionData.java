package cn.org.wangchangjiu.sqltomongo.core.parser.data;

import lombok.Data;

import java.util.List;

@Data
public class CaseExpressionData {
    private List<WhenThenData> whenThenDataList;
    private Object elseValue;
}
