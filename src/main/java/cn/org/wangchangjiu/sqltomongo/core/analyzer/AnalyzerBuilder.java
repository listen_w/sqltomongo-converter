package cn.org.wangchangjiu.sqltomongo.core.analyzer;

import cn.org.wangchangjiu.sqltomongo.core.analyzer.table_option.CreateTableColumnAnalyzer;

/**
 * @Classname AnalyzerBuilder
 * @Description
 * @Date 2022/12/1 16:29
 * @Created by wangchangjiu
 */
public interface AnalyzerBuilder {

    Analyzer newSelectAnalyzerInstance();

    CreateTableColumnAnalyzer newCreateTableAnalyzerInstance();
}
