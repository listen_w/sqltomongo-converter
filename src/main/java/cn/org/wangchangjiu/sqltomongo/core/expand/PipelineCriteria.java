package cn.org.wangchangjiu.sqltomongo.core.expand;

import org.bson.Document;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * @Classname PipelineCriteria
 * @Description
 * @Date 2022/11/30 10:51
 * @Created by wangchangjiu
 */
public class PipelineCriteria {

    private LinkedHashMap<String, Object> criteria = new LinkedHashMap<String, Object>();

    public PipelineCriteria expr(Object expr) {
        this.criteria.put("$expr", expr);
        return this;
    }

    /**
     * 构建相等pipeline表达式
     * @param left 表达式左边
     * @param right 表达式右边
     * @return { "$expr": { "$eq": ["$left", "$right"] } }
     */
    public PipelineCriteria eqExpr(Object left, Object right) {
        HashMap<Object, Object> map = new HashMap<>();
        map.put("$eq",Arrays.asList(left,right));
        this.expr(map);
        return this;
    }


    public Document toDocument() {
        return new Document(criteria);
    }

}
