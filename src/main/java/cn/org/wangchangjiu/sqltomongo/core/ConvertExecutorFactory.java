package cn.org.wangchangjiu.sqltomongo.core;

import cn.org.wangchangjiu.sqltomongo.core.analyzer.DefaultAnalyzerBuilder;
import cn.org.wangchangjiu.sqltomongo.core.exception.SqlTypeException;
import cn.org.wangchangjiu.sqltomongo.core.parser.select_option.DefaultPartSQLParserBuilder;
import cn.org.wangchangjiu.sqltomongo.core.parser.select_option.SelectSQLTypeParser;
import cn.org.wangchangjiu.sqltomongo.core.parser.table_option.CreateTableSQLTypeParser;
import cn.org.wangchangjiu.sqltomongo.core.util.SqlCommonUtil;


public class ConvertExecutorFactory {
    ConvertExecutor getExecutor(SqlCommonUtil.SqlType type){
        if(type == SqlCommonUtil.SqlType.SELECT){
            return new SelectSQLTypeParser(new DefaultPartSQLParserBuilder(), new DefaultAnalyzerBuilder());
        }else if(type== SqlCommonUtil.SqlType.CREATETABLE){
            return new CreateTableSQLTypeParser(new DefaultAnalyzerBuilder());
        }else{
            throw new SqlTypeException("SQL类型暂不支持");
        }
    }
}
