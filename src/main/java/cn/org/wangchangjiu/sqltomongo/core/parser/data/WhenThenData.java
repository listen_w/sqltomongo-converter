package cn.org.wangchangjiu.sqltomongo.core.parser.data;

import lombok.Data;

/**
 * case表达式中的when then组合
 */
@Data
public class WhenThenData {
    private MatchExpression matchExpression;
    private Object thenValue;
}
