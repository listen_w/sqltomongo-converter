package cn.org.wangchangjiu.sqltomongo.core.parser.select_option;

import cn.org.wangchangjiu.sqltomongo.core.parser.data.PartSQLParserData;
import net.sf.jsqlparser.statement.select.PlainSelect;

/**
 * @Classname PartSQLParser
 * @Description
 * @Date 2022/8/12 15:26
 * @Created by wangchangjiu
 */
public abstract class PlainSelectPartSQLParser {
    protected PartSQLParserBuilder builder;

    /**
     * 设置子查询的处理器构筑器
     * @param builder b
     */
    public void setSubSelectParserBuilder(PartSQLParserBuilder builder){
        this.builder = builder;
    }

    public abstract void proceedData(PlainSelect plainSelect, PartSQLParserData data);
}
