package cn.org.wangchangjiu.sqltomongo.core.parser.data;

import cn.org.wangchangjiu.sqltomongo.core.common.SortDirection;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class SortData implements Serializable {

    /**
     *  排序
     */
    private SortDirection direction;

    /**
     *  排序字段
     */
    private String field;

    /**
     *  having的分组集合名
     */
    private String groupCollectionName;


    public SortData(){}

    public SortData(SortDirection direction, String field ,String groupCollectionName){
        this.direction = direction;
        this.field = field;
        this.groupCollectionName = groupCollectionName;
    }

    public Object getOriginExpressionField() {
        return field;
    }

    public void setGroupCollectionName(String name) {
        this.groupCollectionName = name;
    }

    public String getField(){
        return isGroupField()?(groupCollectionName + "." + field):field;
    }

    private boolean isGroupField() {
        return groupCollectionName!=null && !groupCollectionName.isEmpty();
    }
}
