package cn.org.wangchangjiu.sqltomongo.core.parser.select_option;

import cn.org.wangchangjiu.sqltomongo.core.common.ParserPartTypeEnum;

/**
 * @Classname DefaultPartSQLParserBuilder
 * @Description
 * @Date 2022/12/1 17:06
 * @Created by wangchangjiu
 */
public class DefaultPartSQLParserBuilder implements PartSQLParserBuilder {
    @Override
    public PlainSelectPartSQLParser getPartSQLParserInstance(ParserPartTypeEnum typeEnum) {
        return ParserPartTypeEnum.buildNoPlugin(typeEnum);
    }
}
