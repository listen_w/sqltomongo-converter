package cn.org.wangchangjiu.sqltomongo.core.analyzer.select_option;

import cn.org.wangchangjiu.sqltomongo.core.parser.data.LookUpData;
import cn.org.wangchangjiu.sqltomongo.core.parser.data.MatchData;
import cn.org.wangchangjiu.sqltomongo.core.parser.data.PartSQLParserData;
import org.apache.commons.collections4.CollectionUtils;
import org.bson.Document;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @Classname HavingAnalyzer
 * @Description
 * @Date 2022/8/12 11:53
 * @Created by wangchangjiu
 */
public class HavingAnalyzer extends MatchAnalyzer {


    @Override
    public void proceed(List<Document> documents, PartSQLParserData data) {

        List<MatchData> havingData = data.getHavingData();
        // 分析 having
        if (!CollectionUtils.isEmpty(havingData)) {
            documents.addAll(analysisMatch(havingData, data.getTableNameManager()));
        }

    }
}
