package cn.org.wangchangjiu.sqltomongo.core.parser.select_option.impl;

import cn.org.wangchangjiu.sqltomongo.core.adapter.MyGroupByVisitorAdapter;
import cn.org.wangchangjiu.sqltomongo.core.parser.data.GroupData;
import cn.org.wangchangjiu.sqltomongo.core.parser.data.PartSQLParserData;
import cn.org.wangchangjiu.sqltomongo.core.parser.select_option.PlainSelectPartSQLParser;
import net.sf.jsqlparser.statement.select.GroupByElement;
import net.sf.jsqlparser.statement.select.PlainSelect;

import java.util.ArrayList;
import java.util.List;

public class GroupSQLParserPlainSelect extends PlainSelectPartSQLParser {

    @Override
    public void proceedData(PlainSelect plain, PartSQLParserData data) {
        GroupByElement groupBy = plain.getGroupBy();
        if(groupBy != null){
            List<GroupData> groupData = new ArrayList<>();
            groupBy.accept(new MyGroupByVisitorAdapter(groupData));
            data.setGroupData(groupData);
        }
    }
}
