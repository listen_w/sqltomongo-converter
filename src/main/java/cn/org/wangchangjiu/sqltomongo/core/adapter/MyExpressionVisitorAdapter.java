package cn.org.wangchangjiu.sqltomongo.core.adapter;

import cn.org.wangchangjiu.sqltomongo.core.analyzer.select_option.ExpressionAnalyzer;
import cn.org.wangchangjiu.sqltomongo.core.common.AggregationFunction;
import cn.org.wangchangjiu.sqltomongo.core.common.ParserPartTypeEnum;
import cn.org.wangchangjiu.sqltomongo.core.exception.NotSupportFunctionException;
import cn.org.wangchangjiu.sqltomongo.core.parser.data.CaseExpressionData;
import cn.org.wangchangjiu.sqltomongo.core.parser.data.MatchData;
import cn.org.wangchangjiu.sqltomongo.core.util.SqlCommonUtil;
import cn.org.wangchangjiu.sqltomongo.core.util.SqlSupportedSyntaxCheckUtil;
import net.sf.jsqlparser.expression.*;
import net.sf.jsqlparser.expression.ExpressionVisitorAdapter;
import net.sf.jsqlparser.expression.operators.conditional.AndExpression;
import net.sf.jsqlparser.expression.operators.conditional.OrExpression;
import net.sf.jsqlparser.expression.operators.relational.*;
import net.sf.jsqlparser.schema.Column;

import java.util.ArrayList;
import java.util.List;

/**
 * @Classname MyExpressionVisitorAdapter
 * @Description
 * @Date 2022/11/23 17:56
 * @Created by wangchangjiu
 */
public class MyExpressionVisitorAdapter extends ExpressionVisitorAdapter {

    private List<MatchData> matchData;

    private int priority = 0;

    private ParserPartTypeEnum part;



    public MyExpressionVisitorAdapter(ParserPartTypeEnum part, List<MatchData> matchData){
        this.part = part;
        this.matchData = matchData;
    }

    private void processLogicalExpression(BinaryExpression expr, String logic) {
        MatchData.OperatorExpressionItem item = new MatchData.OperatorExpressionItem(logic);
        matchData.add(new MatchData(priority, 0, true, item,null,false));

        priority++;
        expr.getLeftExpression().accept(this);
        expr.getRightExpression().accept(this);
        if (priority != 0) {
            priority--;
        }
    }

    @Override
    protected void visitBinaryExpression(BinaryExpression expr) {
        if (expr instanceof ComparisonOperator || expr instanceof LikeExpression) {
            Expression leftExpression = expr.getLeftExpression();
            Expression rightExpression = expr.getRightExpression();
            if (!(rightExpression instanceof Column) && !(leftExpression instanceof Column) && ParserPartTypeEnum.WHERE == part) {
                //报错 暂不支持 where 不支持 函数
                throw new NotSupportFunctionException("where 条件不支持函数操作");
            }

            AggregationFunction function = null;
            if(ParserPartTypeEnum.HAVING == part && (expr.getLeftExpression() instanceof Function || expr.getRightExpression() instanceof Function)){
                Function func;
                if(expr.getLeftExpression() instanceof Function){
                    func = (Function) expr.getLeftExpression();
                }else{
                    func = (Function) expr.getRightExpression();
                }
                function = AggregationFunction.parser(func.getName());
                ExpressionList parameters = func.getParameters();
                List<Expression> expressions = parameters.getExpressions();
                SqlSupportedSyntaxCheckUtil.checkFunctionColumn(func.getName(), expressions);
                // 解析出 函数 和 字段
                leftExpression = expressions.get(0);
            }

            //处理普通表达式，a.id=1
            if( leftExpression instanceof Column){
                Column leftColumn = Column.class.cast(leftExpression);
                String tableAlias = leftColumn.getTable() == null ? null : leftColumn.getTable().getName();
                MatchData.RelationExpressionItem  item = new MatchData.RelationExpressionItem(tableAlias, leftColumn.getColumnName(),
                        expr.getStringExpression(), function, SqlCommonUtil.handleExpressionValue(expr.getRightExpression()));
                matchData.add(new MatchData(priority, 1, false, item,null,ParserPartTypeEnum.HAVING == part));
            }else if(rightExpression instanceof Column){
                Column expressionColumn = Column.class.cast(rightExpression);
                String tableAlias = expressionColumn.getTable() == null ? null : expressionColumn.getTable().getName();
                MatchData.RelationExpressionItem item = new MatchData.RelationExpressionItem(tableAlias, expressionColumn.getColumnName(),
                        expr.getStringExpression(), function, SqlCommonUtil.handleExpressionValue(expr.getLeftExpression()));
                matchData.add(new MatchData(priority, 1, false, item,null,ParserPartTypeEnum.HAVING == part));
            }
            //处理count(*)
            if(function!=null && SqlSupportedSyntaxCheckUtil.isCountFunc(function.name(),leftExpression)){
                MatchData.RelationExpressionItem item = new MatchData.RelationExpressionItem(null, "*",
                        expr.getStringExpression(), function, SqlCommonUtil.handleExpressionValue(expr.getRightExpression()));
                matchData.add(new MatchData(priority, 1, false, item,null,ParserPartTypeEnum.HAVING == part));
            }else if(function!=null && SqlSupportedSyntaxCheckUtil.isCountFunc(function.name(),rightExpression)){
                MatchData.RelationExpressionItem item = new MatchData.RelationExpressionItem(null, "*",
                        expr.getStringExpression(), function, SqlCommonUtil.handleExpressionValue(expr.getLeftExpression()));
                matchData.add(new MatchData(priority, 1, false, item,null,ParserPartTypeEnum.HAVING == part));
            }

            //处理case表达式
//            if( leftExpression instanceof CaseExpression){
//                CaseExpression caseExpression = (CaseExpression)leftExpression;
//                CaseExpressionData caseExpressionData = ExpressionAnalyzer.analyzerCaseExpression(caseExpression);
//
//            }else if(rightExpression instanceof CaseExpression){
//
//            }
        }
    }

    @Override
    public void visit(AndExpression expr) {
        processLogicalExpression(expr, "AND");

    }

    @Override
    public void visit(OrExpression expr) {
        processLogicalExpression(expr, "OR");
    }

    @Override
    public void visit(InExpression expr) {

        if (!(expr.getLeftExpression() instanceof Column)) {
            //报错 暂不支持
            //  throw new RuntimeException()
        }

        ItemsList rightItemsList = expr.getRightItemsList();
        if (rightItemsList instanceof ExpressionList) {

            List<Object> valueList = new ArrayList<>();
            ExpressionList expressionList = (ExpressionList) rightItemsList;
            List<Expression> expressions = expressionList.getExpressions();
            expressions.stream().forEach(expression -> valueList.add(SqlCommonUtil.handleExpressionValue(expression)));

            Column leftColumn = Column.class.cast(expr.getLeftExpression());
            String tableAlias = leftColumn.getTable() == null ? null : leftColumn.getTable().getName();
            MatchData.RelationExpressionItem item = new MatchData.RelationExpressionItem(tableAlias, leftColumn.getColumnName(),
                    expr.isNot()?"NOT IN":"IN", null, valueList);
            matchData.add(new MatchData(priority, 1, false, item,null,false));
        }
    }

    @Override
    public void visit(Parenthesis parenthesis) {
        parenthesis.getExpression().accept(this);
    }
}
