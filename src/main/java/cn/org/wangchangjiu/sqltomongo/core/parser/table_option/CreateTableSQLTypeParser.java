package cn.org.wangchangjiu.sqltomongo.core.parser.table_option;

import cn.org.wangchangjiu.sqltomongo.core.ConvertExecutor;
import cn.org.wangchangjiu.sqltomongo.core.analyzer.Analyzer;
import cn.org.wangchangjiu.sqltomongo.core.analyzer.AnalyzerBuilder;
import cn.org.wangchangjiu.sqltomongo.core.analyzer.table_option.CreateTableColumnAnalyzer;
import cn.org.wangchangjiu.sqltomongo.core.common.MongoParserResult;
import cn.org.wangchangjiu.sqltomongo.core.common.MongoParserResultForCreateTable;
import cn.org.wangchangjiu.sqltomongo.core.util.SqlCommonUtil;
import net.sf.jsqlparser.statement.create.table.CreateTable;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

public class CreateTableSQLTypeParser implements ConvertExecutor {
    private AnalyzerBuilder analyzerBuilder;

    public CreateTableSQLTypeParser(AnalyzerBuilder analyzerBuilder) {
        this.analyzerBuilder = analyzerBuilder;
    }

    @Override
    public String execute(String sql) {
        return mongoAggregationAnalyzer(getPartSQLParserData(sql)).toJsonString();
    }

    @Override
    public Bson executeForBson(String sql) {
        return null;
    }

    public CreateTable getPartSQLParserData(String parameterSql) {
        return SqlCommonUtil.parserCreateTableSql(parameterSql);
    }

    public MongoParserResultForCreateTable mongoAggregationAnalyzer(CreateTable createTable){
        // ====== 下面开始 构建 Mongo API ============
        new ArrayList<>();

        // 使用责任链设计模式开始分析 每个部分 SQL 封装 MongoAPI
        CreateTableColumnAnalyzer analyzer = analyzerBuilder.newCreateTableAnalyzerInstance();
        return analyzer.proceed(createTable);
    }
}
