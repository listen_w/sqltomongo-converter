package cn.org.wangchangjiu.sqltomongo.core.common;

import cn.org.wangchangjiu.sqltomongo.core.parser.select_option.PlainSelectPartSQLParser;
import cn.org.wangchangjiu.sqltomongo.core.parser.select_option.impl.*;

/**
 * @Classname ParserPartType
 * @Description
 * @Date 2022/8/12 11:44
 * @Created by wangchangjiu
 */
public enum ParserPartTypeEnum {

    JOIN,
    GROUP,
    WHERE,
    HAVING,
    LIMIT,
    PROJECT,
    ORDER;


    /**
     *  获取解析器 不带插件
     * @param typeEnum
     * @return
     */
    public static PlainSelectPartSQLParser buildNoPlugin (ParserPartTypeEnum typeEnum){
        PlainSelectPartSQLParser plainSelectPartSQLParser = null;
        if (typeEnum == ParserPartTypeEnum.GROUP) {
            plainSelectPartSQLParser = new GroupSQLParserPlainSelect();
        } else if (typeEnum == ParserPartTypeEnum.HAVING) {
            plainSelectPartSQLParser = new HavingSQLParserPlainSelect();
        } else if (typeEnum == ParserPartTypeEnum.LIMIT) {
            plainSelectPartSQLParser = new LimitSQLParserPlainSelect();
        } else if (typeEnum == ParserPartTypeEnum.PROJECT) {
            plainSelectPartSQLParser = new ProjectSQLParserPlainSelect();
        } else if (typeEnum == ParserPartTypeEnum.ORDER) {
            plainSelectPartSQLParser = new OrderSQLParserPlainSelect();
        } else if (typeEnum == ParserPartTypeEnum.JOIN) {
            plainSelectPartSQLParser = new JoinSQLParserPlainSelect();
        } else if (typeEnum == ParserPartTypeEnum.WHERE) {
            plainSelectPartSQLParser = new WhereSQLParserPlainSelect();
        }
        return plainSelectPartSQLParser;
    }
}
