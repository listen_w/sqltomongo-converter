package cn.org.wangchangjiu.sqltomongo.core;

import org.junit.Test;

public class GroupTest {
    @Test
    public void sumTest() {
        System.out.println("-------------group test---------------");
        String sql ="SELECT a.str_column,sum(a.id) as sum_id FROM table_a as a group by a.str_column";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void joinSumTest() {
        System.out.println("-------------group test---------------");
        String sql ="SELECT a.str_column,sum(a.id) as sum_aid,sum(b.id) as sum_bid FROM table_a as a left join table_b as b on a.id=b.a_id group by a.str_column";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }
}
