package cn.org.wangchangjiu.sqltomongo.core;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class SimpleTest {

    @Before
    public void setUp() {
        // 在每个测试方法之前执行的代码
//        System.out.println("Setting up before the test.");
    }

    @After
    public void tearDown() {
        // 在每个测试方法之后执行的代码
//        System.out.println("Tearing down after the test.");
    }

    @Test
    public void likeTest() {
        System.out.println("-------------like test---------------");
        String sql ="SELECT * FROM table_a as a WHERE a.str_column LIKE '%3%' or a.str_column like '_____1'";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void sumTest() {
        System.out.println("-------------sum test---------------");
        String sql ="SELECT sum(a.id) as sum_id FROM table_a as a group by a.str_column";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test(expected = ArithmeticException.class)
    public void testDivisionByZero() {
        // 测试会抛出 ArithmeticException
        int result = 1 / 0;
    }
}
