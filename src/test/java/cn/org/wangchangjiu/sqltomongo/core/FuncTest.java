package cn.org.wangchangjiu.sqltomongo.core;

import org.junit.Test;

public class FuncTest {

    @Test
    public void sumTest() {
        System.out.println("-------------sum test---------------");
        String sql ="select a.str_column,sum(a.id) as num from table_a as a group by a.str_column having sum(a.id)>1";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void countTest() {
        // fixme: having中不支持count(*)--select 中的count(*)别名为空
        System.out.println("-------------select test---------------");
        String sql ="select a.str_column,count(a.id),count(b.id),count(*) from table_a as a left join table_b as b on a.id=b.a_id group by a.str_column having count(a.id)>0 and count(*)<100";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void maxMinTest() {
        System.out.println("-------------select test---------------");
        String sql ="select a.str_column,max(a.id),min(a.id) from table_a as a group by a.str_column having max(a.id)>0 and min(a.id)>0";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void avgTest() {
        System.out.println("-------------avg test---------------");
        String sql ="select a.str_column,avg(a.id) from table_a as a group by a.str_column having avg(a.id)>0";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }
}
