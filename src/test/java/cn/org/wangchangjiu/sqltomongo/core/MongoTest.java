package cn.org.wangchangjiu.sqltomongo.core;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.bson.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.mongodb.core.MongoTemplate;

public class MongoTest {
    MongoTemplate mongoTemplate;

    @Before
    public void setUp() {
        // 在每个测试方法之前执行的代码
        MongoClient mongoClient = MongoClients.create("mongodb://admin:123@192.168.9.137:27017/admin");
        mongoTemplate = new MongoTemplate(mongoClient, "test_convert_db");
    }

    @Test
    public void test() {
        String sql = "select a.str_column,count(a.id) as num,max(case when a.id in(1,2,3) then a.id else 0 end) as case_add from table_a as a left join table_b as b on a.id=b.a_id group by a.str_column ORDER BY a.str_column";
        Document command = (Document) Converter.executeForBson(sql);
        System.out.println(Converter.execute(sql));
        System.out.println(command);
        System.out.println("result:"+ mongoTemplate.executeCommand(command));
    }
}
