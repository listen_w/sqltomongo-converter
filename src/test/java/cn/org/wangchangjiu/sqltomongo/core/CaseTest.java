package cn.org.wangchangjiu.sqltomongo.core;

import org.junit.Test;

public class CaseTest {

    @Test
    public void selectEqTest() {
        System.out.println("-------------select eq test---------------");
        String sql ="select a.id,case when a.id = 3 then 1 else 0 end as caseW from table_a as a";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void selectLikeTest() {
        System.out.println("-------------select like test---------------");
        String sql ="select a.str_column,case when a.str_column like '%3' then 1 else 0 end as like3,case when a.str_column like '_____1' then 1 else 0 end as like1 from table_a as a\n";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void selectInTest() {
        System.out.println("-------------select in test---------------");
        String sql ="select a.id,case when a.id in(1,2,3) then 1 else 0 end as in3,case when a.id not in(1,2,3) then 1 else 0 end as nin3 from table_a as a";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void selectLtTest() {
        System.out.println("-------------select lt test---------------");
        String sql ="select a.id,case when a.id <3 then 1 else 0 end as lt3,case when a.id <=3 then 1 else 0 end as lte3 from table_a as a";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void selectGtTest() {
        System.out.println("-------------select gt test---------------");
        String sql ="select a.id,case when a.id >3 then 1 else 0 end as lt3,case when a.id >=3 then 1 else 0 end as lte3 from table_a as a";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void selectNeTest() {
        System.out.println("-------------select ne test---------------");
        String sql ="select a.id,case when a.id != 3 then 1 else 0 end as nea from table_a as a";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void selectRevertTest() {
        System.out.println("-------------select revert test---------------");
        String sql ="select a.id,case when 3=a.id then 1 else 0 end as caseW from table_a as a";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void nonElseTest() {
        System.out.println("-------------select non else test---------------");
        String sql ="select a.id as num,case when a.id=3 then 1 end as cas_non_not3 from table_a as a";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void multipleWhenThenTest() {
        System.out.println("-------------multiple When then test---------------");
        String sql ="select a.id as num,case when a.id=3 or a.id=2 then '2 or 3' when a.id=4 and a.str_column like '%value%' then '4 and str like value' else 'xxx' end as cas_id from table_a as a";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void thenElseValueTest() {
        System.out.println("-------------select then else value test---------------");
        String sql ="select a.id as num,case when a.id not in(1,2,3) then a.id else b.id end as case_id from table_a as a left join table_b as b on a.id=b.a_id";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void selectSumTest() {
        System.out.println("-------------select select sum test---------------");
        String sql ="select a.str_column,count(a.id) as num,sum(case when a.id not in(1,2,3) then 1 else 0 end) as case_add from table_a as a group by a.str_column";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void selectCountTest() {
        System.out.println("-------------select select count test---------------");
        String sql ="select a.str_column,count(a.id) as num,count(case when a.id in(1,2,3) then 1 else null end) as case_add from table_a as a group by a.str_column";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void selectAvgTest() {
        System.out.println("-------------select select avg test---------------");
        String sql ="select a.str_column,count(a.id) as num,avg(case when a.id in(1,2,3) then a.id else 0 end) as case_add from table_a as a group by a.str_column";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void selectMinTest() {
        System.out.println("-------------select select min test---------------");
        String sql ="select a.str_column,count(a.id) as num,min(case when a.id in(1,2,3) then a.id else 0 end) as case_add from table_a as a group by a.str_column";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void selectMaxTest() {
        System.out.println("-------------select select max test---------------");
        String sql ="select a.str_column,count(a.id) as num,max(case when a.id in(1,2,3) then a.id else 0 end) as case_add from table_a as a group by a.str_column";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void havingFuncTest() {
        // TODO: 2024/9/6 暂不支持
        System.out.println("-------------select having func test---------------");
        String sql ="select a.str_column from table_a as a group by a.str_column having sum(case when a.id not in(1,2,3) then 1 else 0 end)>0";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void selectConstTest() {
        // TODO: 2024/9/5 暂不支持 两个静态值
        System.out.println("-------------select const test---------------");
        String sql ="select a.id,case when 3=3 then 1 else 0 end as caseW from table_a as a";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void selectSubSelectInTest() {
        // TODO: 2024/9/5 暂不支持
        System.out.println("-------------select sub select in test---------------");
        String sql ="select a.id,case when a.id in(select b.id from table_b as b) then 1 else 0 end as inb from table_a as a";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void selectEqKHTest() {
        System.out.println("-------------select eq kh test---------------");
        // TODO: 2024/9/5 case when不能加括号
        String sql2 ="select a.id,(case when a.id > 3 then 1 else 0 end) as caseW from table_a as a";
        System.out.println("sql:\n"+sql2);
        System.out.println("mongo:\n"+ Converter.execute(sql2));
        System.out.println("-------------------------------------");
    }
}
