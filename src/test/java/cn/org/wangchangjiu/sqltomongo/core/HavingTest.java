package cn.org.wangchangjiu.sqltomongo.core;

import org.junit.Test;

public class HavingTest {

    @Test
    public void havingTest() {
        System.out.println("-------------select having test---------------");
        String sql ="SELECT a.str_column,sum(a.id) as sum_id FROM table_a as a GROUP BY a.str_column HAVING a.str_column>'value2'";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void funcTest() {
        System.out.println("-------------select having func test---------------");
        String sql ="SELECT a.str_column FROM table_a as a GROUP BY a.str_column HAVING sum(a.id)>3";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void doubleFuncTest() {
        System.out.println("-------------select having func test---------------");
        String sql ="SELECT a.str_column,sum(a.id) FROM table_a as a GROUP BY a.str_column HAVING sum(a.id)>3";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void aliasTest() {
        System.out.println("-------------select having func test---------------");
        String sql ="select a.str_column,count(a.str_column) as aa from table_a as a group by a.str_column having aa>2";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void tableAliasTest() {
        System.out.println("-------------select having func test---------------");
        String sql ="select a.str_column from table_a as a group by a.str_column having a.str_column>'value2'";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void joinFuncTest() {
        System.out.println("-------------select having func test---------------");
        String sql ="select a.str_column,count(a.str_column) as aa from table_a as a left join table_b as b on a.id=b.a_id group by a.str_column having aa>3";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }

    @Test
    public void joinTest() {
        System.out.println("-------------select having func test---------------");
        String sql ="select a.str_column,sum(b.id) from table_a as a left join table_b as b on a.id=b.a_id group by a.str_column having a.str_column < 'value4' and sum(b.id)>0 and sum(a.str_column)>=0";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql));
        System.out.println("-------------------------------------");
    }
}
