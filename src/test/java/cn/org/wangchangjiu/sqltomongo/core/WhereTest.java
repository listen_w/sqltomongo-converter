package cn.org.wangchangjiu.sqltomongo.core;

import org.junit.Test;

public class WhereTest {

    @Test
    public void eqTest(){
        String sql ="select * from table_a as a where a.id=1";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql)+"\n");
    }

    @Test
    public void aliasTest(){
        String sql ="select * from table_a as a where id=1";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql)+"\n");
    }

    @Test
    public void eqRevertTest(){
        String sql ="select * from table_a as a where 1=a.id";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql)+"\n");
    }

    @Test
    public void joinTest(){
        String sql ="select * from table_a as a left join table_b as b on a.id=b.a_id where a.id=1 and b.id=1";
        System.out.println("sql:\n"+sql);
        System.out.println("mongo:\n"+ Converter.execute(sql)+"\n");
    }


    @Test
    public void whereTest() {
        String sql3 ="select * from table_a as a where a.id>1";
        System.out.println("sql:\n"+sql3);
        System.out.println("mongo:\n"+ Converter.execute(sql3)+"\n");

//        String sql ="select * from table_a as a where a.id=1 or 2=a.id";
//        System.out.println("sql:\n"+sql);
//        System.out.println("mongo:\n"+ Converter.execute(sql)+"\n");
//
//        String sql2 ="select * from table_a as a join table_b as b on a.id=b.a_id where a.id=1 or b.id=a.id";
//        System.out.println("sql:\n"+sql2);
//        System.out.println("mongo:\n"+ Converter.execute(sql2));
        System.out.println("-------------------------------------");
    }
}
